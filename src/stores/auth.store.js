import { defineStore } from 'pinia';
import {router} from '../router/index'

import axios from 'axios';

const baseUrl = `http://localhost:8080`;

export const useAuthStore = defineStore({
    id: 'auth',
    state: () => ({
        // initialize state from local storage to enable user to stay logged in
        user: JSON.parse(localStorage.getItem('user')),
        returnUrl: null
    }),
    actions: {
        async login(username, password) {

            
            const user = await axios.post(`http://localhost:8080/authenticate`, {"username": username,"password": password });
        
            // maj du store
            this.user = user;
            console.log(user)
            // permet de garder les infos quand les pages son rafraichie
            localStorage.setItem('user', JSON.stringify(user));

            // redirection a la page precedente ou a l'accueil
            router.push(this.returnUrl || '/');
        },
        logout() {
            this.user = null;
            localStorage.removeItem('user');
            router.push('/login');
        }
    }
});