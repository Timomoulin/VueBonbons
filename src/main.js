import { createApp } from "vue";
import { createPinia } from "pinia";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min.js";

import App from "./App.vue";
import router from "./router";
import axiosApi from 'axios';

const axios = axiosApi.create({
    baseURL:`http://localhost:8080/bonbons`,
    
});

window.axios = axios;


const app = createApp(App);

app.use(createPinia());
app.use(router);

app.mount("#app");
